Edge-AI Project Pages
#####################

Edge-AI is the name given to a set of code and documentation to help use TI accelerators to perform deep learning tasks efficiently.

`This page <https://edge-ai.beagleboard.io>`_ provides background and links to enable collaboration around this code and documentation.

Getting started
===============

Some good starting points:

* `Edge-AI issue boards <https://openbeagle.org/groups/edge-ai/-/boards>`_
* `Edge-AI usage instruction in BeagleBoard.org Debian images <https://edge-ai.beagleboard.io/docs/latest/intro/beagle101/edge-ai.html>`_
* `What is BeagleBoard.org Debian? <https://edge-ai.beagleboard.io/docs/latest/intro/beagle101/debian.html>`_
* `BeagleBoard.org Debian packaging documentation <https://edge-ai.beagleboard.io/docs/latest/intro/contribution/debian-packaging.html>`_
* `Rebuilding Edge-AI instructions for BeagleBoard.org Debian <https://edge-ai.beagleboard.io/docs/latest/intro/contribution/edge-ai-rebuild.html>`_


Useful contribution resources
=============================

* `Git usage <https://docs.beagleboard.org/latest/intro/contribution/git-usage.html>`_
* `reStructured Text Cheat Sheet <https://docs.beagleboard.org/latest/intro/contribution/rst-cheat-sheet.html>`_
* `Documentation Style Guide <https://docs.beagleboard.org/latest/intro/contribution/style.html>`_

License
=======

This work is licensed under a `Creative Commons Attribution-ShareAlike 4.0 International License <http://creativecommons.org/licenses/by-sa/4.0/>`_. Attribute original work to BeagleBoard.org Foundation.

.. image:: https://licensebuttons.net/l/by-sa/4.0/88x31.png
    :width: 88px
    :height: 31px
    :align: left
    :target: http://creativecommons.org/licenses/by-sa/4.0/

